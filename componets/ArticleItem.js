import articleStyles from '../styles/Article.module.css'
import Link from 'next/link'
import React from 'react'
import { Londrina_Sketch } from 'next/font/google'

const ArticleItem = ({article}) => {
  return (
    <Link href='/article/[id]' legacyBehavior as = {`/article/${article.id}`} >
        <a className={articleStyles.card}>
            <h3>{article.title} &rarr;</h3>
            <p>{article.body}</p>

        </a>

    </Link>
  )
}

export default ArticleItem